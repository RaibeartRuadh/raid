#!/bin/bash

mdconf_path="/etc/mdadm.conf"
raid_drives=$(lsblk -dpno NAME | grep -v "$(ls /dev/sd*[0-9] | head -c 8)")
# зачищаем суперблок
mdadm --zero-superblock --force $(echo "$raid_drives")
# тип RAID -l5 и количество дисков в RAID -n3 
mdadm --create --verbose /dev/md0 -l5 -n3 $(echo "$raid_drives")

echo "DEVICE partitions" > $mdconf_path && mdadm --detail --scan --verbose | awk '/ARRAY/ {print}' >> $mdconf_path
# размещаем флаг gpt
parted -s /dev/md0 mklabel gpt
# тип файловой системы ext4

parted -s /dev/md0 mkpart primary ext4 0% 20%
parted -s /dev/md0 mkpart primary ext4 21% 40%
parted -s /dev/md0 mkpart primary ext4 41% 60%
parted -s /dev/md0 mkpart primary ext4 61% 80%
parted -s /dev/md0 mkpart primary ext4 81% 100%

mkfs.ext4 /dev/md0p1
