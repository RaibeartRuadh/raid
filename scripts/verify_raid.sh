#!/bin/bash

# Выводим информацию по блочным системам командой lsblk
printf "\nall devices\n\n\n" && lsblk
# Выводим состояние активных RAID
echo "mdstat: $(mdadm -D /dev/md0)"
# Информация о смонтированных RAID
echo "mounted partition: $(mount | grep /raid)"

